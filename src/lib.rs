extern crate vecmath;
extern crate plain_theme;

pub mod renderer;
//pub mod sub_renderer;

pub use plain_theme::{ Rect, Color, RectShape, point_in_rect, max_size, rect_intersection };
pub use renderer::*;
//pub use sub_renderer::*;
