use plain_theme::*;

use std::marker::Sized;

use renderer::*;

pub struct SubRenderer<'a, R: 'a + ?Sized> {
    pub renderer: &'a mut R,
    pub offset: [i64; 2],
    pub area: Area,
}

impl<'a, R: 'a> SubRenderer<'a, R> {
    pub fn new(renderer: &'a mut R, offset: [i64; 2], area: Area) -> Self {
        SubRenderer {
            renderer,
            area,
            offset
        }
    }

    pub fn new_area(&self, area: Area) -> Area {
        combine_areas(self.area, offset_area(area, self.offset))
    }
}

impl<'a, R: Renderer> Renderer for SubRenderer<'a, R> {
    fn clear(&mut self, area: Area, color: Color) {
        let area = self.new_area(area);
        self.renderer.clear(area, color);
    }

    fn polygon(&mut self, area: Area, polygon: &[[i64; 2]], color: Color) {
        let area = self.new_area(area);
        let polygon = offset_polygon(polygon, self.offset);

        self.renderer.polygon(area, &polygon, color);
    }

    fn line(
        &mut self,
        area: Area,
        line: [[i64; 2]; 2],
        color: Color,
        radius: u64,
        shape: LineShape
    )
    {
        let area = self.new_area(area);
        let line = offset_line(line, self.offset);

        self.renderer.line(area, line, color, radius, shape);
    }

    fn rect(
        &mut self,
        area: Area,
        rect: Rect,
        color: Color,
        shape: RectShape,
        border_color: Color,
        border_radius: u64
    )
    {
        let area = self.new_area(area);
        let rect = offset_rect(rect, self.offset);

        self.renderer.rect(area, rect, color, shape, border_color, border_radius);
    }
}

impl<'a, T: Size, R: 'a + TextureRenderer<T>> TextureRenderer<T> for SubRenderer<'a, R> {
    fn texture(&mut self, area: Area, rect: Rect, texture: &T) {
        let area = self.new_area(area);
        let rect = offset_rect(rect, self.offset);

        self.renderer.texture(area, rect, texture);
    }
}

impl<'a, T: TextSize, R: 'a + TextRenderer<T>> TextRenderer<T> for SubRenderer<'a, R> {
    fn text(&mut self, area: Area, rect: Rect, font: &T, text: &str, color: Color) {
        let area = self.new_area(area);
        let rect = offset_rect(rect, self.offset);

        self.renderer.text(area, rect, font, text, color);
    }
}
